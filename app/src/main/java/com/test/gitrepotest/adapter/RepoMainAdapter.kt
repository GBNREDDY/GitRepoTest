package com.test.gitrepotest.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.test.gitrepotest.R
import com.test.gitrepotest.activity.RepoActivity
import com.test.gitrepotest.model.RepoMain
import kotlin.collections.ArrayList

/**
 * Created by admin on 1/30/2018.
 */

class RepoMainAdapter(private var c: Context, private var layoutInflater: LayoutInflater, private var data: ArrayList<RepoMain>) : RecyclerView.Adapter<RepoMainAdapter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = layoutInflater.inflate(R.layout.item_repo_home, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        holder.name.text = data[position].name
        holder.fname.text = data[position].fullName
        holder.wcount.text = data[position].watchers.toString()
        holder.ccount.text = data[position].commits.toString()+"+"
        Picasso.with(c).load(data[position].avatarUrl).error(R.drawable.ic_launcher_background).into(holder.pimg)

        holder.itemView.setOnClickListener {
            val intent = Intent(c, RepoActivity::class.java)
            intent.putExtra("name",data[position].name)
            intent.putExtra("fname",data[position].fullName)
            intent.putExtra("desc",data[position].description)
            intent.putExtra("wurl",data[position].htmlUrl)
            intent.putExtra("curl",data[position].contributorsUrl)
            intent.putExtra("img",data[position].avatarUrl)
            c.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }


    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var name: TextView = itemView.findViewById(R.id.name) as TextView
        internal var fname: TextView = itemView.findViewById(R.id.fname) as TextView
        internal var wcount: TextView = itemView.findViewById(R.id.wtcount) as TextView
        internal var ccount: TextView = itemView.findViewById(R.id.cmtcount) as TextView
        internal var pimg: ImageView = itemView.findViewById(R.id.proimg) as ImageView
    }
}
