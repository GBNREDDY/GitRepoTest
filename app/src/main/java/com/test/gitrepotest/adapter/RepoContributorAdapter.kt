package com.test.gitrepotest.adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.test.gitrepotest.R
import com.test.gitrepotest.activity.RepoListActivity
import com.test.gitrepotest.model.Contributor


/**
 * Created by admin on 1/30/2018.
 */

class RepoContributorAdapter(private var c: Context, private var layoutInflater: LayoutInflater, private var data: ArrayList<Contributor>?/*, private var data: ArrayList<RepoMain>*/) : RecyclerView.Adapter<RepoContributorAdapter.Holder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = layoutInflater.inflate(R.layout.item_contributor, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.ccount.text = data!![position].name
        Picasso.with(c).load(data!![position].avatarUrl).error(R.drawable.ic_launcher_background).into(holder.pimg)
        holder.itemView.setOnClickListener {
            val intent = Intent(c, RepoListActivity::class.java)
            intent.putExtra("pimg", data!![position].avatarUrl)
            intent.putExtra("pname", data!![position].name)
            intent.putExtra("repurl", data!![position].reposUrl)
            c.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return data!!.size
    }


    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var ccount: TextView = itemView.findViewById(R.id.name) as TextView
        internal var pimg: ImageView = itemView.findViewById(R.id.proimg) as ImageView
    }
}
