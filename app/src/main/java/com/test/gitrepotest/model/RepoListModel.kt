package com.test.gitrepotest.model

import java.io.Serializable

class RepoListModel : Serializable {

    var name: String? = null
    var fullName: String? = null
    var avatharurl: String? = null
    var svn_url: String? = null
    val description: String? = null
    val contributors_url: String? = null
}
