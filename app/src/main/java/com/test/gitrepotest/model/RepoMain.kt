package com.test.gitrepotest.model

import java.io.Serializable

/**
 * Created by admin on 1/31/2018.
 */
class RepoMain: Serializable,Comparable<RepoMain> {
    override fun compareTo(other: RepoMain): Int {
        return other.watchers!!.compareTo(this.watchers!!)
    }

    var name: String? = null
    var fullName: String? = null
    var avatarUrl: String? = null
    var commitsUrl: String? = null
    var htmlUrl: String? = null
    var description: String? = null
    var contributorsUrl: String? = null
    var forks: Int? = null
    var watchers: Int? = null
    var commits: Int? = null

}