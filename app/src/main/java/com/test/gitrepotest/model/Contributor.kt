package com.test.gitrepotest.model

import java.io.Serializable


class Contributor : Serializable {

    var name: String? = null
    var avatarUrl: String? = null
    var reposUrl: String? = null
}
