package com.test.gitrepotest.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.test.gitrepotest.R
import com.test.gitrepotest.adapter.RepoListAdapter
import com.test.gitrepotest.model.RepoMain
import com.test.gitrepotest.networking.RepoListTask
import org.json.JSONArray

class RepoListActivity : AppCompatActivity() {
    private lateinit var recycler: RecyclerView
    private lateinit var mdata: ArrayList<RepoMain>
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repo_list)

        val data = intent
        val pimg = data.getStringExtra("pimg")
        val pname = data.getStringExtra("pname")
        val repurl = data.getStringExtra("repurl")

        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        val mTitle = findViewById<TextView>(R.id.toolbar_title)
        setSupportActionBar(toolbar)
        mTitle.text = pname
        val iv = toolbar.findViewById<View>(R.id.backpress) as ImageView
        iv.visibility = View.VISIBLE
        iv.setOnClickListener { onBackPressed() }
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        val img = findViewById<ImageView>(R.id.pimg)
        Picasso.with(this).load(pimg).error(R.drawable.ic_launcher_background).into(img)

        mdata = ArrayList()
        recycler = findViewById(R.id.recycler_repolist)

        val mtask = RepoListTask(this, repurl)
        mtask.execute()
    }

    private fun refreshrecycler(mdata: ArrayList<RepoMain>) {
        recycler.invalidate()
        recycler.setHasFixedSize(true)
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val ytbadptr = RepoListAdapter(this, layoutInflater, mdata)
        recycler.adapter = ytbadptr
    }


    fun formatdata(s: String) {
        mdata = ArrayList()
        try{
        val root = JSONArray(s)
        for (i in 0..(root.length() - 1)) {
            val item = root.getJSONObject(i)
            val temp = RepoMain()
            temp.name = item.getString("name")
            temp.fullName = item.getString("full_name")
            temp.description = item.getString("description")
            temp.contributorsUrl = item.getString("contributors_url")
            temp.htmlUrl = item.getString("html_url")
            val owner = item.getJSONObject("owner")
            temp.avatarUrl = owner.getString("avatar_url")
            mdata.add(temp)
        }

        if (mdata != null && mdata.size > 0 && mdata.isNotEmpty()) {
            refreshrecycler(mdata)
        }
    }catch (e:Exception){
        Log.d("ssltag","$e")
    }
    }
}
