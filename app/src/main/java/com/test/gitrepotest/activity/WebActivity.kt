package com.test.gitrepotest.activity

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.webkit.WebChromeClient
import android.widget.Toast
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.widget.ImageView
import com.test.gitrepotest.R

class WebActivity : AppCompatActivity() {
    private lateinit var webView: WebView
    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web)

        val toolbar = findViewById<View>(R.id.toolbar) as android.support.v7.widget.Toolbar
        setSupportActionBar(toolbar)
        val iv = toolbar.findViewById<View>(R.id.backpress) as ImageView
        iv.visibility = View.VISIBLE
        iv.setOnClickListener { onBackPressed() }
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        val progressBar = ProgressDialog(this)
        progressBar.setMessage("Please wait...")
        progressBar.setCancelable(false)
        progressBar.show()
        progressBar.isIndeterminate = true
        progressBar.setCancelable(false)

        webView = findViewById(R.id.webview)
        webView.loadUrl(intent.getStringExtra("murl"))
        webView.settings.javaScriptEnabled = true
        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                progressBar.show()
                return true
            }

            override fun onReceivedError(view: WebView, request: WebResourceRequest, error: WebResourceError) {
                //Your code to do
                Toast.makeText(this@WebActivity, "Your Internet Connection May not be active Or " + error, Toast.LENGTH_LONG).show()
                this@WebActivity.finish()
            }
        }

        webView.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, progress: Int) {
                if (progress == 1) {
                    progressBar.show()
                }
                if (progress == 100) {
                    progressBar.dismiss()
                }
            }
        }
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
        if (keyCode == KeyEvent.KEYCODE_BACK && this.webView.canGoBack()) {
            webView.goBack()
            return true
        }
        return super.onKeyDown(keyCode, event)
    }

}
