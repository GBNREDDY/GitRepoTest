package com.test.gitrepotest.activity

import android.app.Dialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomSheetDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.*
import com.test.gitrepotest.R
import com.test.gitrepotest.adapter.RepoMainAdapter
import com.test.gitrepotest.model.RepoMain
import com.test.gitrepotest.networking.CommitsTask
import com.test.gitrepotest.networking.RepoTaskMain
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class HomeActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    private lateinit var recycler: RecyclerView
    private lateinit var data: ArrayList<RepoMain>
    private val orderBy = "desc"
    private val pageNo = 1
    private val perPage = 10
    private var queryBy = "image"
    private val sortBy = "watcher"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById<View>(R.id.toolbar) as android.support.v7.widget.Toolbar
        val filterimg = findViewById<ImageView>(R.id.filter)
        setSupportActionBar(toolbar)

        filterimg.setOnClickListener {
            openDialogue()
        }

        val searchView = findViewById<SearchView>(R.id.search)
        searchView.setOnQueryTextListener(this)
        recycler = findViewById(R.id.recycler_main)

        requestUrl("https://api.github.com/search/repositories?q=$queryBy&per_page=$perPage&sort=$orderBy&order=&page=$pageNo")
    }

    private fun openDialogue() {
        var cal = Calendar.getInstance()
        var sstr = ""
        var fstr = ""
        var tstr = "${cal.get(Calendar.YEAR)}-${String.format("%02d", cal.get(Calendar.MONTH) + 1)}-${String.format("%02d", cal.get(Calendar.DAY_OF_MONTH))}"

        var rstr = "desc"

        val v = layoutInflater.inflate(R.layout.dialogue_filter, null)
        val bstars = v.findViewById<Button>(R.id.stars)
        val bforks = v.findViewById<Button>(R.id.forks)
        val bupdated = v.findViewById<Button>(R.id.updated)

        val bdesc = v.findViewById<Button>(R.id.desc)
        val basc = v.findViewById<Button>(R.id.asc)

        val irefresh = v.findViewById<ImageView>(R.id.refresh)
        val idone = v.findViewById<ImageView>(R.id.done)

        val fdate = v.findViewById<TextView>(R.id.fdate)
        val tdate = v.findViewById<TextView>(R.id.tdate)
        val datell = v.findViewById<LinearLayout>(R.id.datell)

        val pdialogue = BottomSheetDialog(this)
        pdialogue.setContentView(v)
        pdialogue.show()

        bstars.setOnClickListener {
            sstr = "stars"
            bstars.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            bforks.setBackgroundColor(resources.getColor(R.color.colorAccent))
            bupdated.setBackgroundColor(resources.getColor(R.color.colorAccent))
        }

        bforks.setOnClickListener {
            sstr = "forks"
            bforks.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            bstars.setBackgroundColor(resources.getColor(R.color.colorAccent))
            bupdated.setBackgroundColor(resources.getColor(R.color.colorAccent))
        }

        bupdated.setOnClickListener {
            sstr = "updated"
            bupdated.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            bforks.setBackgroundColor(resources.getColor(R.color.colorAccent))
            bstars.setBackgroundColor(resources.getColor(R.color.colorAccent))
        }

        bdesc.setOnClickListener {
            rstr = "desc"
            bdesc.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            basc.setBackgroundColor(resources.getColor(R.color.colorAccent))
        }

        basc.setOnClickListener {
            rstr = "asc"
            basc.setBackgroundColor(resources.getColor(R.color.colorPrimary))
            bdesc.setBackgroundColor(resources.getColor(R.color.colorAccent))
        }

        irefresh.setOnClickListener {
            requestUrl("https://api.github.com/search/repositories?q=image&per_page=$perPage&sort=$sortBy&order=$orderBy&page=$pageNo")
            pdialogue.dismiss()
        }

        idone.setOnClickListener {
            if (tstr != null && sstr != null) {
                requestUrl("https://api.github.com/search/repositories?q=$queryBy:created:<=$tstr&per_page=$perPage&sort=$sstr&order=$rstr&page=$pageNo")
            } else if (sstr != null ) {
                requestUrl("https://api.github.com/search/repositories?q=$queryBy&per_page=$perPage&sort=$sstr&order=$rstr&page=$pageNo")
            }else if (tstr != null ) {
                requestUrl("https://api.github.com/search/repositories?q=$queryBy:created:<=${(tstr)}&sort=$sortBy&per_page=$perPage&order=$rstr&page=$pageNo")
            }else{
                requestUrl("https://api.github.com/search/repositories?q=$queryBy&per_page=$perPage&order=$rstr&page=$pageNo")
            }
            pdialogue.dismiss()
        }

        datell.setOnClickListener {
            val dview = layoutInflater.inflate(R.layout.dialogue_dates, null)
            val fdt = dview.findViewById<Button>(R.id.fromdt)
            val tdt = dview.findViewById<Button>(R.id.todt)

            val cancel = dview.findViewById<Button>(R.id.cancel)
            val ok = dview.findViewById<Button>(R.id.ok)

            val fdtpick = dview.findViewById<DatePicker>(R.id.fromdtpicker)
            val tdtpick = dview.findViewById<DatePicker>(R.id.todtpicker)
            val dialog = Dialog(this)
            dialog.setContentView(dview)
            dialog.show()

            cancel.setOnClickListener {
                dialog.dismiss()
            }

            fdt.setOnClickListener {
                fdt.setBackgroundColor(resources.getColor(R.color.colorAccent))
                tdt.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                tdtpick.visibility = View.GONE
                fdtpick.visibility = View.VISIBLE
            }
            tdt.setOnClickListener {
                fdt.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                tdt.setBackgroundColor(resources.getColor(R.color.colorAccent))
                fdtpick.visibility = View.GONE
                tdtpick.visibility = View.VISIBLE
            }

            ok.setOnClickListener {

                fstr = "${fdtpick.year}-${String.format("%02d", fdtpick.month + 1)}-${String.format("%02d", fdtpick.dayOfMonth)}"
                tstr = "${tdtpick.year}-${String.format("%02d", tdtpick.month + 1)}-${String.format("%02d", tdtpick.dayOfMonth)}"

                tdate.text = tstr
                fdate.text = fstr
                dialog.dismiss()
            }

        }
    }

    override fun onQueryTextChange(p0: String?): Boolean {
        return false
    }

    override fun onQueryTextSubmit(p0: String?): Boolean {
        queryBy = p0!!
        requestUrl("https://api.github.com/search/repositories?q=$p0&per_page=$perPage&sort=$orderBy&order=&page=$pageNo")
//        Toast.makeText(this, p0, Toast.LENGTH_SHORT).show()
        return false
    }

    fun formatdata(result: String?) {
        data = ArrayList()
        try {
            val root = JSONObject(result)
            val itemarray = root.getJSONArray("items")
            for (i in 0 until itemarray.length()) {
                val itemobj = itemarray.getJSONObject(i)
                val name = itemobj.getString("name")
                val fname = itemobj.getString("full_name")
                val owner = itemobj.getJSONObject("owner")
                val p_img = owner.getString("avatar_url")
                val htmlUrl = itemobj.getString("html_url")
                val description = itemobj.getString("description")
                val commitsUrl = itemobj.getString("commits_url")
                val contributorsUrl = itemobj.getString("contributors_url")
                val forks = itemobj.getInt("forks")
                val watchers = itemobj.getInt("watchers_count")
                val mdata = RepoMain()
                mdata.name = name
                mdata.fullName = fname
                mdata.avatarUrl = p_img
                mdata.commitsUrl = commitsUrl
                mdata.htmlUrl = htmlUrl
                mdata.description = description
                mdata.forks = forks
                mdata.contributorsUrl = contributorsUrl
                mdata.watchers = watchers
                data.add(mdata)
                /* val commitsTask = CommitsTask(this, commitsUrl.substring(0,commitsUrl.length-6),i)
                 commitsTask.execute()*/
            }

            Collections.sort(data)
            getCommits(data)
//            Log.d("ssltag", data.size.toString())
            refreshrecycler()
        } catch (e: Exception) {
            Log.d("ssltag", "$e")
        }
        refreshrecycler()
    }

    private fun getCommits(data: ArrayList<RepoMain>) {
        for (i in 0 until  data.size){
            val temp = data[i]
            val mtask = CommitsTask(this, temp.commitsUrl!!.substring(0, temp.commitsUrl!!.length-6), i)
            mtask.execute()
        }
        refreshrecycler()
    }


    private fun requestUrl(s: String) {

        val task = RepoTaskMain(this, s)
        task.execute(s)
    }


    private fun refreshrecycler() {
        recycler.invalidate()
        recycler.setHasFixedSize(true)
        recycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        val ytbadptr = RepoMainAdapter(this, layoutInflater, data)
        recycler.adapter = ytbadptr
    }

    fun addCommits(length: Int, i: Int) {
        val temp = data[i]
        temp.commits = length
        data[i] = temp
        refreshrecycler()
    }


}
