package com.test.gitrepotest.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.test.gitrepotest.R
import com.test.gitrepotest.adapter.RepoContributorAdapter
import com.test.gitrepotest.model.Contributor
import com.test.gitrepotest.networking.ContributorsTask
import org.json.JSONArray
import kotlin.collections.ArrayList


class RepoActivity : AppCompatActivity() {


    private lateinit var recycler: RecyclerView
    private lateinit var mdata: ArrayList<Contributor>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_repo)

        val data = intent

        val name = data.getStringExtra("name")
        val fname = data.getStringExtra("fname")
        val wurl = data.getStringExtra("wurl")
        val desc = data.getStringExtra("desc")
        val curl = data.getStringExtra("curl")
        val img = data.getStringExtra("img")

        val toolbar = findViewById<View>(R.id.toolbar) as android.support.v7.widget.Toolbar
        val mTitle = findViewById<TextView>(R.id.toolbar_title)
        setSupportActionBar(toolbar)
        mTitle.text = name
        val iv = toolbar.findViewById<View>(R.id.backpress) as ImageView
        iv.visibility = View.VISIBLE
        iv.setOnClickListener { onBackPressed() }
        supportActionBar!!.setDisplayShowTitleEnabled(false)



        mdata = ArrayList()
        recycler = findViewById(R.id.recycler_repo)
        val pimg = findViewById<ImageView>(R.id.repoimg)
        val repolink = findViewById<TextView>(R.id.plink)
        val pname = findViewById<TextView>(R.id.pname)
        val pdesc = findViewById<TextView>(R.id.desc)
        repolink.text = wurl
        pname.text = fname
        pdesc.text = desc


        repolink.setOnClickListener {
            val intent = Intent(this, WebActivity::class.java)
            intent.putExtra("murl", wurl)
            startActivity(intent)
        }

        val ctask = ContributorsTask(this, curl)
        ctask.execute()
        Picasso.with(this).load(img).error(R.drawable.ic_launcher_background).into(pimg)

    }

    private fun refreshrecycler(muser: ArrayList<Contributor>?) {
        recycler.invalidate()
        recycler.setHasFixedSize(true)
        recycler.layoutManager = GridLayoutManager(this, 4)
        val ytbadptr = RepoContributorAdapter(this, layoutInflater, muser)
        recycler.adapter = ytbadptr
    }


    fun formatdata(s: String) {
        mdata = ArrayList()
        try {
            val root = JSONArray(s)
            for (i in 0..(root.length() - 1)) {
                val item = root.getJSONObject(i)
                val temp = Contributor()
                temp.name = item.getString("login")
                temp.avatarUrl = item.getString("avatar_url")
                temp.reposUrl = item.getString("repos_url")
                mdata.add(temp)
            }

            if (mdata != null && mdata.size > 0 && mdata.isNotEmpty()) {
                refreshrecycler(mdata)
            }
        }catch (e:Exception){
            Log.d("ssltag","$e")
        }
    }

}
