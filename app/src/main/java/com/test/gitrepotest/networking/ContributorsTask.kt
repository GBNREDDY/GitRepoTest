package com.test.gitrepotest.networking

import android.app.ProgressDialog
import android.content.ContentValues
import android.os.AsyncTask
import android.util.Log
import com.test.gitrepotest.activity.RepoActivity
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

/**
 * Created by admin on 1/31/2018.
 */
class ContributorsTask(private val repoActivity: RepoActivity, private val ustr: String) : AsyncTask<String, String, String>() {
    var jsonStr: String? = null
    private lateinit var pDialog: ProgressDialog
    override fun onPreExecute() {
        super.onPreExecute()
        // Showing progress dialog
        pDialog = ProgressDialog(repoActivity)
        pDialog.setMessage("Please wait...")
        pDialog.setCancelable(false)
        pDialog.show()

    }

    override fun doInBackground(vararg strings: String): String? {
        val sh = HttpHandler()

        // Making a request to url and getting response
        jsonStr = sh.makeServiceCall(ustr)

        Log.e(ContentValues.TAG, "Response from url: " + jsonStr)
        System.out.println("response" + jsonStr)
        return jsonStr
    }

    override fun onPostExecute(s: String?) {
        super.onPostExecute(s)
        Log.e("ssltag", "string : " + s)
        if (pDialog.isShowing)
            pDialog.dismiss()

        if (s != null) {
            repoActivity.formatdata(s)
        }
    }
}