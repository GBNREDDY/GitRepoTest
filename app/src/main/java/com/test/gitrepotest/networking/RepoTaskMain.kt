package com.test.gitrepotest.networking

import android.app.ProgressDialog
import android.os.AsyncTask
import android.util.Log

import android.content.ContentValues.TAG
import com.test.gitrepotest.activity.HomeActivity


/**
 * Created by GBNReddy on 30-01-2018.
 */

/*
https://api.github.com/search/repositories?q=image&per_page=10
*/

class RepoTaskMain(private val homeActivity: HomeActivity?, private val s: String) : AsyncTask<String, String, String>() {
    var jsonStr: String? = null
    private lateinit var pDialog: ProgressDialog

    override fun onPreExecute() {
        super.onPreExecute()
        // Showing progress dialog
        pDialog = ProgressDialog(homeActivity)
        pDialog.setMessage("Please wait...")
        pDialog.setCancelable(false)
        pDialog.show()

    }

    override fun doInBackground(vararg strings: String): String? {
        val sh = HttpHandler()

        // Making a request to url and getting response
        jsonStr = sh.makeServiceCall(s)

        Log.d(TAG, "Response from url: " + jsonStr)
        System.out.println("response" + jsonStr)
        return jsonStr
    }

    override fun onPostExecute(s: String?) {
        super.onPostExecute(s)
        Log.d("ssltag", "string : " + s)
        if (pDialog.isShowing)
            pDialog.dismiss()

        if (s != null) {
            homeActivity!!.formatdata(s)
        }
    }


}
